class Calc:
    def __init__(self):
        pass

    def multiply(self, x, y):
        resUp = x.a * y.a
        resDown = x.b * y.b
        return Rational(resUp, resDown)

    def divide(self, x, y):
        resUp = x.a * y.b
        resDown = x.b * y.a
        return Rational(resUp, resDown)

    def plus(self, x, y):
        resUp = x.a * y.b + y.a * x.b
        resDown = x.b * y.b
        return Rational(resUp, resDown)

    def minus(self, x, y):
        if (y.a * x.b > x.a* y.b):
           resUp = y.a * x.b - x.a * y.b
        resUp = x.a * y.b - y.a * x.b
        resDown = x.b * y.b
        return Rational(resUp, resDown)


    def getResult(self, operation, num1, num2):
        if operation == '*':
            return self.multiply(num1, num2)
        elif operation == '/':
            return self.divide(num1, num2)
        elif operation == '+':
            return self.plus(num1, num2)
        elif operation == '-':
            return self.minus(num1, num2)
        else:
            raise Exception('You must choose the operation!')


class Rational:
    def __init__(self, a, b):
        self.a = a
        self.b = b


    def simplify(self):
        abs_a = abs(self.a)
        abs_b = self.b
        while abs_a != 0 and abs_b != 0:
            if abs_a > abs_b:
                abs_a %= abs_b
            else:
                abs_b %= abs_a
        gcd = abs_a + abs_b
        resUp = self.a/gcd
        resDown = self.b/gcd

        return Rational(resUp, resDown)

    def toFloat(self):
        return float(self.a) / float(self.b)

    def toString(self):
        return 'Rational: {} / {}'.format(int(self.a), int(self.b))


calc = Calc()


num1_str = input('Enter first num: ')
a, b = num1_str.split('/')
num1 = Rational(int(a), int(b))
if num1.b == 0:
    raise Exception('err')

operation = input('Choose the operation type: ')

num2_str = input('Enter second num: ')
k, n = num2_str.split('/')
num2 = Rational(int(k), int(n))
if num2.b == 0:
    raise Exception('err')

res = calc.getResult(operation, num1, num2).simplify()
print('Result: ', res.toString())
print('Result in float format: ', res.toFloat())