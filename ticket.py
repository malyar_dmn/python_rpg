
class StandartTicket():
    def __init__(self):
        self.name = 'Стандартный билет'
        self.ticketId = 1
        self.price = 200

    def getInfo(self):
        print('{} {}'.format(self.name, self.price))


class PrevTicket(StandartTicket):
    def __init__(self):
        super().__init__()
        self.name = 'Авансовый билет'
        self.ticketId = 2
        self.price = self.price - (self.price * 0.4)


class LateTicket(StandartTicket):
    def __init__(self):
        super().__init__()
        self.name = 'Простроченый билет'
        self.ticketId = 3
        self.price = self.price + (self.price * 0.1)

class StudentTicket(StandartTicket):
    def __init__(self):
        super().__init__()
        self.name = 'Студенческий билет'
        self.ticketId = 4
        self.price = self.price - (self.price * 0.5)

class TicketOffice:
    def __init__(self):
        self.ticketList = [StandartTicket(), PrevTicket(), LateTicket(), StudentTicket()]


    def buyTicket(self, inpType):
        if(inpType == 'стандартный'):
            return StudentTicket()
        elif(inpType == 'авансовый'):
            return PrevTicket()
        elif(inpType == 'простроченый'):
            return LateTicket()
        elif(inpType == 'студенческий'):
            return StudentTicket()
        else:
            raise ValueError('Wrong value')


    def getInfoById(self):
        pass

    def getPrice(self, ticket, inpType):
        if(inpType == 'стандартный' or inpType == 'авансовый' or inpType == 'простроченый' or inpType == 'студенческий'):
            print('{} стоит {}'.format(ticket.name, ticket.price))


office = TicketOffice()

inpType = input('Выберите тип билета из предложеных: стандартный, авансовый, простроченый, студенческий\n ')
tick = office.buyTicket(inpType)
office.getPrice(tick, inpType)

