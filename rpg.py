class Hero:
    def __init__(self, race, gender, skinColor, weight, tatoo, hairColor):
        self.race = race
        self.gender = gender
        self.skinColor = skinColor
        self.weight = weight
        self.tatoo = tatoo
        self.hairColor = hairColor


class Human(Hero):
    def __init__(self, *args):
        super().__init__(*args)



class Humanoid(Hero):
    def __init__(self, *args, tatoo = None, hairColor = None):
        super().__init__(*args, tatoo, hairColor)
        self.tatoo = tatoo
        self.hairColor = hairColor


def createHero():


    if(raceProp == 'nord' or raceProp == 'breton' or raceProp == 'dunmer' or raceProp == 'altmer'):
        return Human(raceProp, genderProp, skinColorProp, weightProp, tatooProp, hairColorProp)
    elif(raceProp == 'argonian' or raceProp == 'khadjit'):
        return Humanoid(raceProp, genderProp, skinColorProp, weightProp)



raceProp = input('Enter the race from proposed types: "nord", "breton", "dunmer", "altmer", "argonian", "khadjit"\n')
genderProp = input('Enter the gender of your character: ')
skinColorProp = input('Enter the skin color: ')
weightProp = int(input('Enter the weight: '))
tatooProp = input('Enter the tatoo type: ')
hairColorProp = input('Enter hair color: ')
hero = createHero()


if(hero.__class__.__name__ == 'Human'):
    print('Character created\n'
          'Race: {}\n'
          'Gender: {}\n'
          'Skin color: {}\n'
          'Weight: {}\n'
          'Tatoo: {}\n'
          'Hair color: {}'.format(hero.race, hero.gender, hero.skinColor, hero.weight, hero.tatoo, hero.hairColor))

elif(hero.__class__.__name__ == 'Humanoid'):
    print('Character created\n'
          'Race: {}\n'
          'Gender: {}\n'
          'Skin color: {}\n'
          'Weight: {}'.format(hero.race, hero.gender, hero.skinColor, hero.weight))
