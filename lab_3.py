print('%5d%3d' % (5, 8))

print('%5d%3d' % (10, 20))

# float

print('float with: ', '%2f%10f' % (2.48, 2.38))

print('float with: ', '%2.2f%10.2f' % (2.48, 2.38))

print('float with format: {}, {}; {:.2f}, {:.2f}'.format(2.48, 2.38, 2.487, 2.3832))

print(f'float with f string: {2.34542}')

print(f'float with f string: {2.34542:.2f}')