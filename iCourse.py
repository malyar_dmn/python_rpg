class Course:
    def __init__(self, name, themes, type, teacher):
        self.courseName = name
        self.themes = themes
        self.type = type
        self.teacher = teacher



class Teacher:
    def __init__(self):
        self.name = None
        self.course = None

class MathTeacher(Teacher):
    def __init__(self):
        super().__init__()
        self.name = 'Dima'
        self.course = 'Math'

class EconTeacher(Teacher):
    def __init__(self):
        super().__init__()
        self.name = 'Max'
        self.course = 'Economy'

class CourseFactory:

    def createCourse(self, name, themes, type, teacher):
        return Course(name, themes, type, teacher)


factory = CourseFactory()


courseName = input('Enter course name: ')
courseThemes = input('Enter course themes: ').split(', ')
courseType = input('Course type: ')


course = Course(courseName, courseThemes, courseType, Teacher())

print(course.themes)